<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('lot', '\App\Http\Controllers\Api\LotController')->middleware('checkuser');
Route::resource('bar', '\App\Http\Controllers\Api\BarController')->middleware('checkuser');
Route::get('bar-search', '\App\Http\Controllers\Api\BarController@search');

Route::post('login', '\App\Http\Controllers\Api\AuthController@login');
