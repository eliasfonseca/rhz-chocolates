<?php

namespace Tests\Feature\Http\Controllers\Api;

use App\Models\Lots;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LotControllerTest extends TestCase
{
    /**
     *@test
     */
    public function logged_in_user_can_create_a_lot()
    {
        $token = md5('contact@rhzchocolates.com' . '123456');
        $arrayOrigin = ['Orgânica', 'Pré-processada'];
        $key = array_rand($arrayOrigin, 1);

        $this->withoutExceptionHandling();

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)->json('POST', '/lot', [
            'description'   => $description = 'Test description',
            'supplier'      => $supplier = 'RZH Company',
            'origin'        => $origin = $arrayOrigin[$key]
        ]);

        $response->assertStatus(201);

        $this->assertDatabaseHas('lots', [
            'description'   => $description,
            'supplier'      => $supplier,
            'origin'        => $origin
        ]);

    }

    /**
     *@test
     */
    public function logged_in_user_can_update_a_lot()
    {
        $token = md5('contact@rhzchocolates.com' . '123456');
        $arrayOrigin = ['Orgânica', 'Pré-processada'];
        $key = array_rand($arrayOrigin, 1);

        $this->withoutExceptionHandling();

        $lot = Lots::first();

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)->json('PUT', '/lot/'.$lot->id, [
            'description'   => $description = 'Test description',
            'supplier'      => $supplier = 'RZH Company',
            'origin'        => $origin = $arrayOrigin[$key]
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('lots', [
            'description'   => $description,
            'supplier'      => $supplier,
            'origin'        => $origin
        ]);

    }

    /**
     *@test
     */
    public function logged_in_user_can_delete_a_lot()
    {
        $token = md5('contact@rhzchocolates.com' . '123456');
        $arrayOrigin = ['Orgânica', 'Pré-processada'];
        $key = array_rand($arrayOrigin, 1);

        $this->withoutExceptionHandling();

        $lot = Lots::first();

        $response = $this->withHeader('Authorization', 'Bearer ' . $token)->json('DELETE', '/lot/'.$lot->id, [
            'description'   => $description = 'Test description',
            'supplier'      => $supplier = 'RZH Company',
            'origin'        => $origin = $arrayOrigin[$key]
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseMissing('lots', $lot->toArray());

    }
}
