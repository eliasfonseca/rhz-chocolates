<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lots extends Model
{
    use HasFactory;

    protected $fillable = [
        'description', 'supplier', 'origin'
    ];

    public function bars()
    {
        return $this->belongsToMany(Bars::class, 'bar_lot', 'lot_id', 'bar_id')
        ->withTimestamps()->withPivot('weight_lot');
    }
}
