<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bars extends Model
{
    use HasFactory;

    protected $fillable = [
        'weight', 'code'
    ];

    public function lots()
    {
        return $this->belongsToMany(Lots::class, 'bar_lot', 'bar_id', 'lot_id')
        ->withTimestamps()->withPivot('weight_lot', 'percentage');
    }
}
