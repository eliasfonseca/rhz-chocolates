<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarLot extends Model
{
    use HasFactory;

    protected $fillable = [
        'bar_id', 'lot_id', 'weight_lot', 'percentage'
    ];

}
