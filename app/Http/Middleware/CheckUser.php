<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiControllerTrait;

class CheckUser
{
    use ApiControllerTrait;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // Verificação simples, apenas para simular o bloqueio de rotas.

        $token = md5('contact@rhzchocolates.com' . '123456');

        if ($request->bearerToken() == $token) {
            return $next($request);
        } else {
            return $this->createResponse([
                'message' => 'Sem permissão.',
            ], 422);
        }
    }
}
