<?php

namespace App\Http\Interfaces\Bar;

use Illuminate\Http\Request;

interface IBarChange
{
    function execute(Request $request, int $id);
}