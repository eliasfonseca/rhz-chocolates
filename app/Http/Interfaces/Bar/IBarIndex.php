<?php

namespace App\Http\Interfaces\Bar;

use Illuminate\Http\Request;

interface IBarIndex
{
    function execute(Request $request);
}