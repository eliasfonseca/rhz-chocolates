<?php

namespace App\Http\Interfaces\Bar;

interface IBarDelete
{
    function execute(int $id);
}