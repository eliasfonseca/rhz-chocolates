<?php

namespace App\Http\Interfaces\Bar;

interface IBarShow
{
    function execute(int $id);
}