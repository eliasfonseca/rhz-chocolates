<?php

namespace App\Http\Interfaces\Bar;

use Illuminate\Http\Request;

interface IBarSearch
{
    function execute(Request $request);
}