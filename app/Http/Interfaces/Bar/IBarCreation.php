<?php

namespace App\Http\Interfaces\Bar;

use Illuminate\Http\Request;

interface IBarCreation
{
    function execute(Request $request) : object;
}