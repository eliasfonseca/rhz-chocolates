<?php

namespace App\Http\Interfaces\Lot;

interface ILotShow
{
    function execute(int $id);
}