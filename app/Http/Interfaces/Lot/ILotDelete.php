<?php

namespace App\Http\Interfaces\Lot;

interface ILotDelete
{
    function execute(int $id);
}