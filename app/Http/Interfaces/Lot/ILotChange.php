<?php

namespace App\Http\Interfaces\Lot;

use Illuminate\Http\Request;

interface ILotChange
{
    function execute(Request $request, int $id);
}