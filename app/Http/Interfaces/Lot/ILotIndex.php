<?php

namespace App\Http\Interfaces\Lot;

use Illuminate\Http\Request;

interface ILotIndex
{
    function execute(Request $request);
}