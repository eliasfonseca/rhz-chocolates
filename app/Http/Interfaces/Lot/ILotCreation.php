<?php

namespace App\Http\Interfaces\Lot;

use Illuminate\Http\Request;

interface ILotCreation
{
    function execute(Request $request) : object;
}