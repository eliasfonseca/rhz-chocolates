<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InvalidOriginException;
use App\Exceptions\NotFindException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiControllerTrait;
use App\Http\Interfaces\Lot\ILotChange as LotILotChange;
use App\Http\Interfaces\Lot\ILotCreation as LotILotCreation;
use App\Http\Interfaces\Lot\ILotDelete as LotILotDelete;
use App\Http\Interfaces\Lot\ILotIndex as LotILotIndex;
use App\Http\Interfaces\Lot\ILotShow as LotILotShow;

class LotController extends Controller
{
    use ApiControllerTrait;

    private $lotCreation;
    private $lotChange;
    private $lotDelete;
    private $lotShow;
    private $lotIndex;

    public function __construct(LotILotCreation $lotCreation, LotILotChange $lotChange, LotILotDelete $lotDelete, LotILotShow $lotShow, LotILotIndex $lotIndex)
    {
        $this->lotCreation = $lotCreation;
        $this->lotChange = $lotChange;
        $this->lotDelete = $lotDelete;
        $this->lotShow = $lotShow;
        $this->lotIndex = $lotIndex;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            return $this->createResponse([
                "lots" => $this->lotIndex->execute($request)
            ], 200);

        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rulesIputsLot = $this->getRulesInputsLot();
            $validate = $this->validateInputs($request, $rulesIputsLot);

            $responseValidate = $validate->original['data']['content'];

            if (isset($responseValidate->error)) {
                return $validate;
            }

            return $this->createResponse([
                "message"  => 'Lote cadastrado com sucesso.',
                "lot"  => $this->lotCreation->execute($request)
            ], 201);

        } catch (InvalidOriginException $e) {
            return $this->createResponse([
                "message" => 'Origem não reconhecida, favor informar (Orgânica) ou (Pré-processada).',
                "error" => true
            ], 422);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->createResponse([
                "lot" => $this->lotShow->execute($id)
            ], 200);
            
        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => "Lote não encontrado.",
            ], 404);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $rulesIputsLot = $this->getRulesInputsLot();
            $validate = $this->validateInputs($request, $rulesIputsLot);

            $responseValidate = $validate->original['data']['content'];

            if (isset($responseValidate->error)) {
                return $validate;
            }

            return $this->createResponse([
                "message"  => 'Lote atualizado com sucesso.',
                "lot"  => $this->lotChange->execute($request, $id)
            ], 200);

        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => "Lote não encontrado.",
            ], 404);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            return $this->createResponse([
                "message"  => $this->lotDelete->execute($id)
            ], 200);

        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => "Lote não encontrado.",
            ], 404);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    private function getRulesInputsLot($id = null)
    {
        return [
            'description' => ['required'],
            'supplier' => ['required'],
            'origin' => ['required']
        ];
    }
}
