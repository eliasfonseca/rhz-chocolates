<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\InvalidAmountGramsException;
use App\Exceptions\InvalidCodeException;
use App\Exceptions\InvalidLotsException;
use App\Exceptions\InvalidPercentageWeightException;
use App\Exceptions\NotFindException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiControllerTrait;
use App\Http\Interfaces\Bar\IBarIndex as BarIBarIndex;
use App\Http\Interfaces\Bar\IBarShow as BarIBarShow;
use App\Http\Interfaces\Bar\IBarDelete as BarIBarDelete;
use App\Http\Interfaces\Bar\IBarSearch as BarIBarSearch;
use App\Http\Interfaces\Bar\IBarCreation as BarIBarCreation;
use App\Http\Interfaces\Bar\IBarChange as BarIBarChange;

class BarController extends Controller
{
    use ApiControllerTrait;

    private $barIndex;
    private $barShow;
    private $barDelete;
    private $barCreation;
    private $barChange;

    public function __construct(BarIBarIndex $barIndex, BarIBarShow $barShow, BarIBarDelete $barDelete, BarIBarSearch $barSearch, BarIBarCreation $barCreation, BarIBarChange $barChange)
    {
        $this->barIndex = $barIndex;
        $this->barShow = $barShow;
        $this->barDelete = $barDelete;
        $this->barSearch = $barSearch;
        $this->barCreation = $barCreation;
        $this->barChange = $barChange;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            return $this->createResponse([
                "bars" => $this->barIndex->execute($request)
            ], 200);

        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rulesIputsBar = $this->getRulesInputsBar();
            $validate = $this->validateInputs($request, $rulesIputsBar);

            $responseValidate = $validate->original['data']['content'];

            if (isset($responseValidate->error)) {
                return $validate;
            }

            return $this->createResponse([
                "message"  => 'Barra cadastrada com sucesso.',
                "bar"  => $this->barCreation->execute($request),
            ], 201);

        } catch (InvalidCodeException $e) {
            return $this->createResponse([
                "message" => "O código informado já está sendo usado por outra barra, favor informar outro.",
            ], 422);
        } catch (InvalidLotsException $e) {
            return $this->createResponse([
                "message" => "Favor informar os lotes e suas quantidades em gramas!",
            ], 422);
        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => "Lote não encontrado.",
            ], 404);
        } catch (InvalidAmountGramsException $e) {
            return $this->createResponse([
                "message" => "O peso total da barra não pode ser diferente do padão (500g), verifique o campo amount_grams dos lotes!",
            ], 422);
        } catch (InvalidPercentageWeightException $e) {
            return $this->createResponse([
                "message" => "A barra deve ter no mínimo 90% da composição de cacau orgânico e no máximo 10% da composição de cacau pré-processado.",
            ], 422);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return $this->createResponse([
                "bar" => $this->barShow->execute($id)
            ], 200);

        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => "Barra não encontrada.",
            ], 404);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $rulesIputsBar = $this->getRulesInputsBar();
            $validate = $this->validateInputs($request, $rulesIputsBar);

            $responseValidate = $validate->original['data']['content'];

            if (isset($responseValidate->error)) {
                return $validate;
            }

            return $this->createResponse([
                "message"  => 'Barra atualizada com sucesso.',
                "bar"  => $this->barChange->execute($request, $id)
            ], 201);

        } catch (InvalidCodeException $e) {
            return $this->createResponse([
                "message" => "O código informado já está sendo usado por outra barra, favor informar outro.",
            ], 422);
        } catch (InvalidLotsException $e) {
            return $this->createResponse([
                "message" => "Favor informar os lotes e suas quantidades em gramas!",
            ], 422);
        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => $e->getMessage(),
            ], 404);
        } catch (InvalidAmountGramsException $e) {
            return $this->createResponse([
                "message" => "O peso total da barra não pode ser diferente do padão (500g), verifique o campo amount_grams dos lotes!",
            ], 422);
        } catch (InvalidPercentageWeightException $e) {
            return $this->createResponse([
                "message" => "A barra deve ter no mínimo 90% da composição de cacau orgânico e no máximo 10% da composição de cacau pré-processado.",
            ], 422);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            return $this->createResponse([
                "message"  => $this->barDelete->execute($id),
            ], 200);

        } catch (NotFindException $e) {
            return $this->createResponse([
                "message" => "Lote não encontrado.",
            ], 404);
        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    public function search(Request $request)
    {
        try {
            return $this->createResponse([
                "bars" => $this->barSearch->execute($request)
            ], 200);

        } catch (\Throwable $th) {
            return $this->createResponse([
                "message" => $th->getMessage(),
                "error"   => true
            ], 500);
        }
    }

    private function getRulesInputsBar($id = null)
    {
        return [
            'code' => ['required', 'max:8', 'min:8']
        ];
    }
}
