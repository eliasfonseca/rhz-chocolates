<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Api\ApiControllerTrait;

class AuthController extends Controller
{
    use ApiControllerTrait;

    // Método login fake, apenas para simular o bloqueio de rotas.
    public function login(Request $request) {        
        $token = md5($request->email . $request->password);
        return $this->createResponse([
            "token" => $token
        ], 200);
    }
}
