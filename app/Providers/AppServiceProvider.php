<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Http\Interfaces\Lot\ILotCreation',
            'App\Application\Lot\LotCreation'
        );

        $this->app->bind(
            'App\Http\Interfaces\Lot\ILotChange',
            'App\Application\Lot\LotChange'
        );

        $this->app->bind(
            'App\Http\Interfaces\Lot\ILotDelete',
            'App\Application\Lot\LotDelete'
        );

        $this->app->bind(
            'App\Http\Interfaces\Lot\ILotShow',
            'App\Application\Lot\LotShow'
        );

        $this->app->bind(
            'App\Http\Interfaces\Lot\ILotIndex',
            'App\Application\Lot\LotIndex'
        );

        $this->app->bind(
            'App\Http\Interfaces\Bar\IBarIndex',
            'App\Application\Bar\BarIndex'
        );

        $this->app->bind(
            'App\Http\Interfaces\Bar\IBarShow',
            'App\Application\Bar\BarShow'
        );

        $this->app->bind(
            'App\Http\Interfaces\Bar\IBarDelete',
            'App\Application\Bar\BarDelete'
        );

        $this->app->bind(
            'App\Http\Interfaces\Bar\IBarSearch',
            'App\Application\Bar\BarSearch'
        );

        $this->app->bind(
            'App\Http\Interfaces\Bar\IBarCreation',
            'App\Application\Bar\BarCreation'
        );

        $this->app->bind(
            'App\Http\Interfaces\Bar\IBarChange',
            'App\Application\Bar\BarChange'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
