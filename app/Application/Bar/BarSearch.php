<?php

namespace App\Application\Bar;

use App\Http\Interfaces\Bar\IBarSearch as BarIBarSearch;
use App\Models\Bars;
use Illuminate\Http\Request;

class BarSearch implements BarIBarSearch
{
    function execute(Request $request)
    {
        $weight = $request->weight;
        $code = $request->code;
        $supplier = $request->supplier;
        $weightLot = $request->weightLot;
        $origin = $request->origin;
        $percentage = $request->percentage;

        $query = Bars::query();

        $query->when($weight, function($query) use ($weight) {
            $query->where('weight', $weight);
        });

        $query->when($code, function($query) use ($code) {
            $query->where('code', $code);
        });

        $query->when($supplier, function($query) use ($supplier) {
            $query->whereHas('lots', function ($query) use ($supplier) {
                $query->where('supplier', 'LIKE', '%' . $supplier . '%');
            });
        });

        $query->when($weightLot, function($query) use ($weightLot) {
            $query->whereHas('lots', function ($query) use ($weightLot) {
                $query->where('weight_lot', $weightLot);
            });
        });

        $query->when($origin, function($query) use ($origin) {
            $query->whereHas('lots', function ($query) use ($origin) {
                $query->where('origin', $origin);
            });
        });

        $query->when($percentage, function($query) use ($percentage) {
            $query->whereHas('lots', function ($query) use ($percentage) {
                $query->where('percentage', $percentage);
            });
        });

        return $query->with('lots')->orderBy('id', 'Desc')->paginate(15);
    }
}