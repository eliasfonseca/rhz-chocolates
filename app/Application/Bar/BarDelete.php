<?php

namespace App\Application\Bar;

use App\Exceptions\NotFindException;
use App\Http\Interfaces\Bar\IBarDelete as BarIBarDelete;
use App\Models\Bars;

class BarDelete implements BarIBarDelete
{
    function execute(int $id)
    {
        $bar = Bars::with('lots')->find($id);
        
        if(!$bar) {
            throw new NotFindException();
        }

        foreach ($bar->lots as $key => $lot) {
            $bar->lots()->detach($lot['id']);
        }

        $bar->delete();

        return 'Barra deletada com sucesso.';

    }
}