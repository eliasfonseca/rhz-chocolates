<?php

namespace App\Application\Bar;

use App\Exceptions\NotFindException;
use App\Http\Interfaces\Bar\IBarShow;
use App\Models\Bars;

class BarShow implements IBarShow
{
    function execute(int $id)
    {
        $bar = Bars::with('lots')->where('id', $id)->first();
        
        if (!$bar) {
            throw new NotFindException();
        }
        
        return $bar;
    }
}