<?php

namespace App\Application\Bar;

use App\Http\Interfaces\Bar\IBarIndex as BarIBarIndex;
use App\Models\Bars;
use Illuminate\Http\Request;

class BarIndex implements BarIBarIndex
{
    function execute(Request $request)
    {
        $weight = $request->weight;
        $code = $request->code;

        $query = Bars::query();

        $query->when($weight, function($query) use ($weight) {
            $query->where('weight', $weight);
        });

        $query->when($code, function($query) use ($code) {
            $query->where('code', $code);
        });

        return $query->with('lots')->orderBy('id', 'Desc')->paginate(15);
    }
}