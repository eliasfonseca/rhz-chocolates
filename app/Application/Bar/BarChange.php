<?php

namespace App\Application\Bar;

use App\Exceptions\InvalidAmountGramsException;
use App\Exceptions\InvalidCodeException;
use App\Exceptions\InvalidLotsException;
use App\Exceptions\InvalidOriginException;
use App\Exceptions\InvalidPercentageWeightException;
use App\Exceptions\NotFindException;
use App\Http\Interfaces\Bar\IBarChange as BarIBarChange;
use App\Models\Bars;
use App\Models\Lots;
use Illuminate\Http\Request;

class BarChange implements BarIBarChange
{
    function execute(Request $request, int $id)
    {
        $bar = Bars::find($id);

        if (!$bar) {
            throw new NotFindException('Barra nao encontrada.');
        }
        
        $barExists = Bars::where('code', $request->code)->where('id', '<>', $id)->first();

        if(isset($barExists->id)) {
            throw new InvalidCodeException();
        }

        $lots = $request->lots;

        if($lots == null) {
            throw new InvalidLotsException();
        }

        $totalGrams = 0;
        $totalGramsOrganica = 0;
        $totalGramsPreProcessada = 0;

        foreach ($lots as $key => $lot) {
            $totalGrams += $lot['amount_grams'];

            $lotExists = Lots::find($lot['lot_id']);
            if (!$lotExists) {
                throw new NotFindException('Lote não encontrado.');
            }

            $totalGramsOrganica += $lotExists['origin'] == 'Orgânica' ? $lot['amount_grams'] : 0;
            $totalGramsPreProcessada += $lotExists['origin'] == 'Pré-processada' ? $lot['amount_grams'] : 0;

            $arrayLots[$key] = $lot['lot_id']; 
            $arrayWeightLot[$key] = $lot['amount_grams']; 
        }

        if($totalGrams != 500) {
            throw new InvalidAmountGramsException();
        }

        $percentageOrganica = $totalGramsOrganica / $totalGrams * 100;

        if ($percentageOrganica < 90) {
            throw new InvalidPercentageWeightException();
        }

        $bar->weight = $totalGrams;
        $bar->code = $request->code;
        $bar->save();

        foreach ($lots as $key => $lot) {
            $bar->lots()->detach($lot['lot_id']);
        }

        foreach ($lots as $key => $lot) {
            $percentage = $lot['amount_grams'] / $totalGrams * 100;
            $bar->lots()->attach($lot['lot_id'], ['weight_lot' => $lot['amount_grams'], 'percentage' => $percentage]);
        }

        return $bar;
    }
}