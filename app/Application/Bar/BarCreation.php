<?php

namespace App\Application\Bar;

use App\Exceptions\InvalidAmountGramsException;
use App\Exceptions\InvalidCodeException;
use App\Exceptions\InvalidLotsException;
use App\Exceptions\InvalidPercentageWeightException;
use App\Exceptions\NotFindException;
use App\Http\Interfaces\Bar\IBarCreation as BarIBarCreation;
use App\Models\Bars;
use App\Models\Lots;
use Illuminate\Http\Request;

class BarCreation implements BarIBarCreation
{
    function execute(Request $request): object
    {
        $barExists = Bars::where('code', $request->code)->first();

        if(isset($barExists->id)) {
            throw new InvalidCodeException();
        }

        $lots = $request->lots;

        if($lots == null) {
            throw new InvalidLotsException();
        }

        $totalGrams = 0;
        $totalGramsOrganica = 0;
        $totalGramsPreProcessada = 0;

        foreach ($lots as $key => $lot) {
            $totalGrams += $lot['amount_grams'];

            $lotExists = Lots::find($lot['lot_id']);
            if (!$lotExists) {
                throw new NotFindException();
            }

            $totalGramsOrganica += $lotExists['origin'] == 'Orgânica' ? $lot['amount_grams'] : 0;
            $totalGramsPreProcessada += $lotExists['origin'] == 'Pré-processada' ? $lot['amount_grams'] : 0;
        }

        if($totalGrams != 500) {
            throw new InvalidAmountGramsException();
        }

        $percentageOrganica = $totalGramsOrganica / $totalGrams * 100;

        if ($percentageOrganica < 90) {
            throw new InvalidPercentageWeightException();
        }

        $bar = new Bars();
        $bar->weight = $totalGrams;
        $bar->code = $request->code;
        $bar->save();

        foreach ($lots as $key => $lot) {
            $percentage = $lot['amount_grams'] / $totalGrams * 100;
            $bar->lots()->attach($lot['lot_id'], ['weight_lot' => $lot['amount_grams'], 'percentage' => $percentage]);
        }

        return $bar;
    }
}