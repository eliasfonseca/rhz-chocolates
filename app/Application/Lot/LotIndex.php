<?php

namespace App\Application\Lot;

use App\Http\Interfaces\Lot\ILotIndex as LotILotIndex;
use App\Models\Lots;
use Illuminate\Http\Request;

class LotIndex implements LotILotIndex
{
    function execute(Request $request)
    {
        $supplier = $request->supplier;
        $origin = $request->origin;

        $query = Lots::query();

        $query->when($supplier, function($query) use ($supplier) {
            $query->where('supplier', 'LIKE', '%' . $supplier . '%');
        });

        $query->when($origin, function($query) use ($origin) {
            $query->where('origin', $origin);
        });

        return $query->with('bars')->orderBy('id', 'Desc')->paginate(15);
    }
}