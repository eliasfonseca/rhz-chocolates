<?php

namespace App\Application\Lot;

use App\Exceptions\NotFindException;
use App\Http\Interfaces\Lot\ILotDelete as LotILotDelete;
use App\Models\Lots;

class LotDelete implements LotILotDelete
{
    function execute(int $id)
    {
        $lot = Lots::with('bars')->find($id);

        if(!$lot) {
            throw new NotFindException();
        }
        
        foreach ($lot->bars as $key => $bar) {
            $lot->bars()->detach($bar['id']);
        }

        $lot->delete();

        return 'Lote deletado com sucesso.';
    }
}