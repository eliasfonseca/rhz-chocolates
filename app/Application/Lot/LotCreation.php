<?php

namespace App\Application\Lot;

use App\Exceptions\InvalidOriginException;
use App\Http\Interfaces\Lot\ILotCreation as LotILotCreation;
use App\Models\Lots;
use Illuminate\Http\Request;

class LotCreation implements LotILotCreation
{
    function execute(Request $request): object
    {
        $arrayOrigin = ['Orgânica', 'Pré-processada'];

        if(!in_array($request->origin, $arrayOrigin)) {
            throw new InvalidOriginException();
        }

        $lot = new Lots();
        $lot->description = $request->description;
        $lot->supplier = $request->supplier;
        $lot->origin = $request->origin;

        $lot->save();

        return $lot;
    }
}