<?php

namespace App\Application\Lot;

use App\Exceptions\NotFindException;
use App\Http\Interfaces\Lot\ILotShow as LotILotShow;
use App\Models\Lots;

class LotShow implements LotILotShow
{
    function execute(int $id)
    {
        $lot = Lots::with('bars')->where('id', $id)->first();

        if (!$lot) {
            throw new NotFindException();
        }

        return $lot;
    }
}