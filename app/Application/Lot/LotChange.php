<?php

namespace App\Application\Lot;

use App\Exceptions\InvalidOriginException;
use App\Exceptions\NotFindException;
use App\Http\Interfaces\Lot\ILotChange as LotILotChange;
use App\Models\Lots;
use Illuminate\Http\Request;

class LotChange implements LotILotChange
{
    function execute(Request $request, int $id)
    {
        $arrayOrigin = ['Orgânica', 'Pré-processada'];

        if(!in_array($request->origin, $arrayOrigin)) {
            throw new InvalidOriginException();
        }

        $lot = Lots::find($id);

        if (!$lot) {
            throw new NotFindException();
        }

        $lot->description = $request->description;
        $lot->supplier = $request->supplier;
        $lot->origin = $request->origin;

        $lot->save();

        return $lot;
    }
}