<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BarLot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bar_lot', function (Blueprint $table) {
            $table->id();
            $table->foreignId('bar_id');
            $table->foreignId('lot_id');
            $table->float('weight_lot');
            $table->integer('percentage');
            $table->timestamps();

            $table->foreign('bar_id')->references('id')->on('bars');
            $table->foreign('lot_id')->references('id')->on('lots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bar_lot');
    }
}
