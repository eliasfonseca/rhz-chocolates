<?php

namespace Database\Seeders;

use App\Models\Bars;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bars = [
            [
                'weight' => 500,
                'code' => 12345678,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        foreach ($bars as $bar) {
            DB::table('bars')->insert([
                'weight'        => $bar['weight'],
                'code'          => $bar['code'], 
                'created_at'    => $bar['created_at'],
                'updated_at'    => $bar['updated_at']
            ]);
        }

    }
}
