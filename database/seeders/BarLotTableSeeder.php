<?php

namespace Database\Seeders;

use App\Models\Bars;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BarLotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lots = [
            [
                'lot_id' => 1,
                'amount_grams' => 400
            ],
            [
                'lot_id' => 1,
                'amount_grams' => 50
            ],
            [
                'lot_id' => 2,
                'amount_grams' => 50
            ]
        ];

        $totalGrams = 0;
        foreach ($lots as $key => $lot) {
            $totalGrams += $lot['amount_grams'];
        }

        foreach ($lots as $key => $lot) {
            $percentage = $lot['amount_grams'] / $totalGrams * 100;
            $bar = Bars::first();
            $bar->lots()->attach($lot['lot_id'], ['weight_lot' => $lot['amount_grams'], 'percentage' => $percentage]);
        }
    }
}
