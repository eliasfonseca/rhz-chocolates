<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LotTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lots = [
            [
                'description' => 'Primeiro Lote de Cacau Orgânico',
                'supplier' => 'RZM Kakau',
                'origin' => 'Orgânica',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'description' => 'Segundo Lote de Cacau Orgânico',
                'supplier' => 'RZM Organic',
                'origin' => 'Orgânica',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'description' => 'Terceiro Lote de Cacau Orgânico',
                'supplier' => 'RZM Foods Brazil',
                'origin' => 'Pré-processada',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];

        foreach ($lots as $lot) {
            DB::table('lots')->insert([
                'description'   => $lot['description'],
                'supplier'      => $lot['supplier'], 
                'origin'        => $lot['origin'],
                'created_at'    => $lot['created_at'],
                'updated_at'    => $lot['updated_at']
            ]);
        }
    }
}
